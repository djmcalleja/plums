class Pool extends Phaser.Group {
	constructor(spriteType, name) {
		/* I call super (Phaser.Group) on which the Pool is extended.*/
		super(game, game.world, name);
		this.game = game;
		this.spriteType = spriteType; // Needed when creating new objects in the pool
		return this;
	}
  
	create(data) {
		// Find the first child that has a false exist property:
		let obj = this.getFirstExists(false);
		if (!obj) {
			// We failed to find an available child, so we create one now and add it to the pool.
			obj = new this.spriteType(data);
			this.add(obj, true);
			var new_ = true;
		}
		/* We call the childs spawn method and return the object to whatever triggered this.
		 The spawn method will handle stuff like position, resetting the health property
		 and setting exists to true. The spawned object will live even if the returned
		 reference is ignored:*/
		//obj.resetPoint.set(x,y);
		if (!new_) {
			obj.exists = true;
			obj.reset_(data, true);
		}
		return obj;
	}
}
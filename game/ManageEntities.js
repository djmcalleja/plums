// Entity utility functions and managing creating/destroying
function entityInit() {
	entities = [];

	backEntities = game.add.group(undefined, "backEntities");
	// Only back are displayed behind tiles
	midEntities = game.add.group(undefined, "midEntities");
	throwables = game.add.group(undefined, "throwables");
	frontEntities = game.add.group(undefined, "frontEntities");

	entities.push(backEntities);
	entities.push(midEntities);
	entities.push(throwables);
	entities.push(frontEntities);
	
	player1 = new Player(0,0);
	player1.initialize();
	midEntities.add(player1);
	
	// Controls
	setupKeys(player1);
	
	createPools();
	currentDoor = null;
}

function createPools() {
	ballPool = new Pool(Ball, 0, "Ball");
	doorPool = new Pool(Door, 0, "Door");
	keyPool = new Pool(Key, 0, "Key");
	spikePool = new Pool(Spike, 0, "Spike");
	springPool = new Pool(Spring, 0, "Spring");
	cratePool = new Pool(Crate, 0, "Crate");
}

function getHigherPriority(sprite1, sprite2) {
	// For grabbing things
	// Key is highest priority
	if ((s=sprite1).name === 'Key' || (s=sprite2).name === 'Key')
		return s;
	return sprite1;
}

function createObject(o) {
	var s=m = undefined;
	if (o.properties)
			var reverse = o.properties.rev;
	if (o.type == 'Door') {
		var reverse = false;
		if (o.properties)
			var reverse = o.properties.rev;
		s=backEntities.add(doorPool.create({x:o.x, y:o.y-o.height, w:o.width,h:o.height, rev:reverse}));
		currentDoor = s;
	} else if (o.type == 'Gumpy') {
		if (player1.needsPosition) {
			player1.position.set(o.x+16, o.y-64*0.375);
			player1.needsPosition = false;
		}
		player1.resetPoint.set(o.x+16, o.y-64*0.375);
		return false;
	} else if (o.type == 'Apricot') {
		s=midEntities.add(new Apricot(o.x, o.y, reverse, player1));
	} else if (o.type == 'Boxka') {
		midEntities.add(new Boxka(o.x, o.y, reverse, player1));
	} else if (o.type == 'Crate') {
		s=throwables.add(cratePool.create({x:o.x, y:o.y}));
	} else if (o.type == 'Key') {
		s=throwables.add(keyPool.create({x:o.x, y:o.y}));
	} else if (o.type == 'Ball') {
		s=throwables.add(new Ball(o.x, o.y));
	} else if (o.type == 'Spike') {
		if ((p=o.properties))
			m=backEntities.add(spikePool.create({x:o.x, y:o.y, w:o.width, h:o.height, flippedX:p.flippedx, flippedY:p.flippedy, inTime:p.in, outTime:p.out, delay:p.delay}));
		else
			m=backEntities.add(spikePool.create({x:o.x, y:o.y, w:o.width, h:o.height, flippedX:false,flippedY:false}));
	} else if (o.type == 'Spring') {
		s=backEntities.add(new Spring(o.x, o.y-16));
		s.initialize(o.properties.power);
	}

	if (s) {
		// Adjust entities by their anchors
		// Also increase height since they are drawn downwards from top
		s.position.set(s.x+s.body.width*s.anchor.x, s.y-(s.body.height*s.anchor.y));
		if (s.resetPoint)
			s.resetPoint.set(s.x, s.y);
		return s;
	} else if (m) {
		// Just for spikes
		m.position.set(m.x+16+m.body.width*m.anchor.x, m.y-(m.body.height*m.anchor.y));
		if (m.resetPoint)
			m.resetPoint.set(m.x, m.y);
		return m;
	}
	
}

function fadeOutObjects() {
	player1.dropItem(1.5);
	roomObjsTemp = [player1];
	roomObjects.forEach(function(o) {
		if (o.name != 'Player')
			if (!fadeOutSingleObject(o)) {
				// These should stay in roomObjects
				roomObjsTemp.push(o);
			}
	});
	roomObjects = roomObjsTemp;
}

function fadeOutSingleObject(o) {
	t = game.add.tween(o).to( { alpha: 0 }, 500, "Linear", true, 1000);
	if (o.name == 'Door') {
		if (!o.oldDoor) {
			o.oldDoor = true;
			return false;
		}
	}
	t.onComplete.add(exterminateObject, o);
	return true;
}

function exterminateObject(o) {
	o.exists = false;
	o.body.enable = false;
	// Return to pool
	if (o.name == "Ball") {
		ballPool.add(o);
	} else if (o.name == "Door") {
		doorPool.add(o);
	} else if (o.name == "Key") {
		o.sparkler.on = false;
		keyPool.add(o);
	} else if (o.name == "Spike") {
		o.stopTweens();
		spikePool.add(o);
	} else if (o.name == "Spring") {
		springPool.add(o);
	}
}

function resetObjectPositions() {
	var allDone = true;
	// rs - reset speed
	roomObjects.forEach(function(o) {
		if (o.name == 'Spike')
			return;
		if (!o.resetPoint)
			return;
		var rs = (Math.abs(o.resetPoint.x-o.x) + Math.abs(o.resetPoint.y-o.y)) / 30;
		if (rs < 1)
			rs = 1
		if (o.x < o.resetPoint.x) {
			if (o.x + rs >= o.resetPoint.x)
				o.x = o.resetPoint.x;
			else
				o.x += rs;
				notDone = false;
		} else if (o.x > o.resetPoint.x) {
			if (o.x - rs <= o.resetPoint.x)
				o.x = o.resetPoint.x;
			else
				o.x -= rs;
		} else if (o.y > o.resetPoint.y) {
			if (o.y - rs <= o.resetPoint.y)
				o.y = o.resetPoint.y;
			else
				o.y -= rs;
		} else if (o.y < o.resetPoint.y) {
			if (o.y + rs >= o.resetPoint.y)
				o.y = o.resetPoint.y;
			else
				o.y += rs;
		}
		
		if (!(o.x == o.resetPoint.x && o.y == o.resetPoint.y))
			allDone = false;
	});
	
	if (allDone) {
		resetting = false;
		roomObjects.forEach(function(o) {
			o.body.moves = true;
		});
		restartCooldown = 2.5;
	}
}

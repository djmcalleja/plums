Boxka.prototype = Object.create(Enemy.prototype);
Boxka.prototype.constructor = Boxka;

function Boxka(x, y, rev, player) {
	Enemy.call(this, x, y-(46-32), 'boxka');
	this.name = 'Boxka';
	this.init();
	this.player = player;

	this.crateSpeed = 200;
	this.nakedSpeed = 260;

	this.body.maxVelocity.x = this.nakedSpeed;
	/*if (rev)
		this.changeDirection("left");
	else
		this.changeDirection("right");
	*/
	
	// Create crate
	this.crate = this.addChild(cratePool.create({x:0, y:7}));
	this.crate.body.enable = false;
}

Boxka.prototype.update = function() {
	if (this.player.x > this.x - 300 && this.body.blocked.down) {
		this.body.velocity.y = -800;
	}
	
	if (this.crate)
		this.crate.update();
}
function Throwable(x, y, spriteName) {
	Phaser.Sprite.call(this, game, x, y, spriteName);
	this.resetPoint = {
		x: x, y: y,
		set: function(nx, ny) {
			this.x = nx;
			this.y = ny;
		}
	};
}

Throwable.prototype = Object.create(Phaser.Sprite.prototype);
// Switch from sprite's constructor to throwable?
Throwable.prototype.constructor = Throwable;

Throwable.prototype.init = function(width,height,gravity) {	
	game.physics.arcade.enable(this);
	this.body.collideWorldBounds = true;
	this.body.maxVelocity.y = 650;
	
	if (gravity)
		this.body.gravity.y = gravity;
	else
		this.body.gravity.y = 850;
	
	// Default throwspeed - used for crate
	this.throwSpeedX = 200;
	this.throwSpeedY = 300;
	
	// Hold offset
	this.holdOffsetX = 0;
	this.holdOffsetY = 0;
	
	// Throw safety net - should be larger for faster/bigger objects
	this.throwSafetyNet = 8;
	
	// Default flip - when held, always match player's direction
	// crateflip - don't change direction when picked up, but change when player turns while holding
	this.crateFlip = false;
	
	if (width != 'undefined' && width != null)
		this.width = width;
	if (height != 'undefined' && height != null)
		this.height = height;
		
	this.anchor.setTo(0.5, 0.5);
}

Throwable.prototype.setAnimation = function() {
	
}

Throwable.prototype.update = function() {
	
}

Throwable.prototype.move = function() {
	
}

Throwable.prototype.getName = function() {
	return "Throwable";
}

Throwable.prototype.justThrown = function() {
	
}

Throwable.prototype.reset_ = function() {
	this.body.reset(this.x, this.y);
}

Throwable.prototype.kill = function() {
	this.alive = false;
}

/*
Throwable.prototype.prepareGroundCollision = function() {
	
}

Throwable.prototype.prepareEntityCollision = function() {
	
}*/
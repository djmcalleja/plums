function Enemy(x, y, spriteName) {
	Phaser.Sprite.call(this, game, x, y, spriteName);
	this.resetPoint = {
		x: x, y: y,
		set: function(nx, ny) {
			this.x = nx;
			this.y = ny;
		}
	};
}

Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;

Enemy.prototype.init = function(width,height,gravity) {	
	game.physics.arcade.enable(this);
	this.body.collideWorldBounds = true;
	this.body.maxVelocity.y = 650;
	
	if (gravity)
		this.body.gravity.y = gravity;
	else
		this.body.gravity.y = 850;

	if (width != 'undefined' && width != null)
		this.width = width;
	if (height != 'undefined' && height != null)
		this.height = height;
		
	this.anchor.setTo(0.5, 0.5);
}

Enemy.prototype.reset_ = function() {
	this.body.reset(this.x, this.y);
}

Enemy.prototype.kill = function() {
	this.alive = false;
}
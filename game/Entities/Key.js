Key.prototype = Object.create(Throwable.prototype);
Key.prototype.constructor = Key;

function Key(data) {
	Throwable.call(this, data.x, data.y, 'key');
	var x = data.x; var y = data.y;
	this.init();
	this.name = 'Key';
	this.body.bounce.x = 0.1;
	this.throwSpeedX = 100;
	this.throwSpeedY = 175;
	this.anchor.setTo(0.4, 0.5);
	this.holdOffsetX = -5;
	this.holdOffsetY = 2;
	// Too slow to need this, also has smaller collision right after
	// being thrown
	this.throwSafetyNet = 0;
	
	// When thrown, it will be size 32x32 so it won't go through walls.
	// It will then grow to expand after a second.
	// Increase if it's possible to throw the key sideways through a wall
	this.expandDelay = 0.1;
	
	this.sparkler = game.add.emitter(this.x, this.x);
	this.sparkler.width = this.width;
    this.sparkler.makeParticles('sparkle');
	this.sparkler.setAlpha(0.2, 1, 100);
	this.sparkler.maxParticleSpeed.x = 10;
	this.sparkler.maxParticleSpeed.y = 10;
	this.sparkler.minParticleSpeed.x = 3;
	this.sparkler.minParticleSpeed.y = 3;
	this.sparkler.particleDrag.x = 2;
	this.sparkler.particleDrag.y = 2;
	this.sparkler.gravity = -20;
    this.sparkler.setScale(0.5, 0.9, 0.5, 0.9, 2500, undefined, true);
	
	this.explodeSparkler = game.add.emitter(this.x, this.x);
	this.explodeSparkler.width = this.width;
    this.explodeSparkler.makeParticles('sparklebig');
	this.explodeSparkler.setAlpha(0.2, 1, 100);
	this.explodeSparkler.maxParticleSpeed.x = 80;
	this.explodeSparkler.maxParticleSpeed.y = -200;
	this.explodeSparkler.minParticleSpeed.x = -80;
	this.explodeSparkler.minParticleSpeed.y = 0;
	this.explodeSparkler.particleDrag.x = 2;
	this.explodeSparkler.particleDrag.y = 2;
	this.explodeSparkler.gravity = 150;
    this.explodeSparkler.setScale(0.5, 0.9, 0.5, 0.9, 2500, undefined, true);

    this.sparkler.start(false, 3000, 150, 0);
	
	// To check if there is space for key to rotate
	this.angleSprite = game.add.sprite();
	game.physics.arcade.enable(this.angleSprite);
	this.angleSprite.body.width = 32;
	this.angleSprite.body.height = 96;
	this.angleSprite.anchor.setTo(0.5, 0.5);
}

Key.prototype.update = function() {
	if (this.body.blocked.down || this.body.touching.down)
	{
		// Make sure this isn't the value
		// when key is thrown
		this.body.drag.x = 5000;
	} else {
		this.body.drag.x = 0;
	}

	this.sparkler.x = this.x;
	this.sparkler.y = this.y;
	if (this.beingHeld) {
		this.sparkler.x += this.parent.x;
		this.sparkler.y += this.parent.y;
	}
	
	if (this.openDoorDelay > 0) {
		if (this.explode) {
			this.explodeSparkler.start(true, 10000, 1, 200, false);
			this.explode = false;
			this.kill();
		}
		this.openDoorDelay -= game.time.elapsed / 1000;
		if (this.openDoorDelay <= 0) {
			currentDoor.setHasKey(true);
		}
	}

	if (this.expandDelay > 0 && !this.beingHeld) {
		this.expandDelay -= game.time.elapsed / 1000;
		if (this.expandDelay <= 0) {
			game.add.tween(this.body).to( { width: 64 }, 500, "Linear", true);
		}
	}
	
	if (this.beingHeld)
		this.angleSprite.position.setTo(this.x+this.parent.x, this.y-48+this.parent.y);	
}

Key.prototype.pickedUp = function() {
	// Default angle so the size is set
	this.body.width = 24;
	this.defaultAngle();
	this.rotation = 0.2;
	this.beingHeld = true;
}

Key.prototype.justThrown = function(direction) {
	this.beingHeld = false;
	// Call when thrown, because when picked up the drag is high for one cycle
	this.body.drag.x = 0;
	if (direction === 'up' && this.canThrowUp())
		this.angleUp();
	else if (direction === 'down')
		this.angleDown();
	else {
		this.expandDelay = 0.1;
		this.defaultAngle();	
	}
}

Key.prototype.putInDoor = function() {
	var dissolve = false;
	if (currentDoor.isVertical()) {
		if (this.rotation == 0 || this.rotation == 0.2)
			dissolve = true;
	} else {
		// If above door, must be facing down, and vice versa
		if (this.y >= currentDoor.y) {
			// Above
			if (this.rotation == -90 * (Math.PI/180)) {
				dissolve = true;
			}
		} else {
			// Below
			if (this.rotation == 90 * (Math.PI/180)) {
				dissolve = true;
			}
		}
	}
	if (dissolve) {
		this.openDoorDelay = 2;
		this.body.enable = false;
		this.dissolve();
	}
}

Key.prototype.dissolve = function() {
	this.explodeSparkler.x = this.x;
	this.explodeSparkler.y = this.y;
	this.explode = true;
}

Key.prototype.angleUp = function() {
	this.rotation = -90 * (Math.PI/180);
	this.anchor.setTo(0.5, 0.5);
	this.body.width = 32;
	this.body.height = 64;
	this.scale.x = 1;
	this.x += 6;
}

Key.prototype.angleDown = function() {
	this.rotation = 90 * (Math.PI/180);
	this.anchor.setTo(0.5, 0.5);
	this.y -= 5;
	this.body.width = 32;
	this.body.height = 64;
	this.scale.x = 1;
	this.expandDelay = 0;
	this.x += 6;
}

Key.prototype.canThrowUp = function() {	
	var x = this.angleSprite.body.x;
	var y = this.angleSprite.y;
	var w = this.angleSprite.body.width;
	var h = this.angleSprite.height;
	var tees = tiles.getTiles(this.angleSprite.x-200,this.angleSprite.y-200,400,400,true,true);
	for (i = 0; i < tees.length; ++i) {
		if (tees[i].intersects(x, y-h, x+w, y))
			return false;
	}
	return true;
}

Key.prototype.defaultAngle = function() {
	this.body.height = 32;
	this.rotation = 0;
	this.anchor.setTo(0.4, 0.5);
}

Key.prototype.kill = function() {
	this.alpha = 0;
	this.body.enable = false;
	this.sparkler.on = false;
}

Key.prototype.reset_ = function(data, resetPos=false) {
	if (resetPos) {
		this.x = data.x;
		this.y = data.y;
	}
	this.body.reset(this.x, this.y);
	this.defaultAngle();
	this.scale.x = 1;
	this.body.enable = true;
	this.openDoorDelay = 0;
	this.alpha = 1;
	this.sparkler.on = true;
}
Apricot.prototype = Object.create(Enemy.prototype);
Apricot.prototype.constructor = Apricot;

function Apricot(x, y, rev, player) {
	Enemy.call(this, x, y, 'apricot');
	this.init();
	this.name = 'Apricot';
	this.player = player;
	
	PATROL = 0;
	CHARGING = 1;
	ROLLING = 2;
		
	this.spriteEdge = game.add.sprite(this.x + this.width, this.y + this.height/2-1); //, 'crate');
	game.physics.arcade.enable(this.spriteEdge);
	this.spriteEdge.width = 24;
	this.spriteEdge.height = 1;
	this.spriteEdge.name = "ApricotEdge";
	this.spriteEdge.body.gravity.y = 100;
	this.spriteEdge.onPlatform = true;
	midEntities.add(this.spriteEdge);
	
	/* Direction should be set in tiled */
	//this.direction = "right";
	this.originalRev = rev;
	this.walkSpeed = 100;

	this.body.maxVelocity.x = this.walkSpeed;
	if (rev)
		this.changeDirection("left");
	else
		this.changeDirection("right");

	this.animations.add('walk', [1,2,3,4], 6, true);
	this.animations.add('roll', [5], 1, false);
	this.animations.play('walk');
	
	this.setState(PATROL);
	
	// Edge delay: Give a moment to allow the edge sprite to fall to the ground before used
	this.edgeDelay = 0.25;
	// Sight delay: After turning, there is a period where ap will not notice the player
	this.sightDelay = 0.5;
	// Wall timer: after walking into wall for this amount of time, will turn around.
	this.wallTimer = 0.1;
	this.bounceCooldown = 0;
}

Apricot.prototype.update = function() {
	if (this.bounceCooldown > 0)
		this.bounceCooldown -= game.time.elapsed/1000;
	
	if (this.body.moves)
		switch (this.state) {
		case PATROL:
			this.movementNormal();
			break;
		case CHARGING:
			this.movementCharging();
			break;
		case ROLLING:
			this.movementRolling();
			break;
		}
}

Apricot.prototype.bounceBack = function(left, strength, bouncer) {
	if (this.bounceCooldown > 0)
		return;
	this.bounceCooldown = 0.1;
	this.bounced = true;
	var mod = 1;
	if (left)
		mod = -1;
	this.body.y -= 1;
	if (strength > 1)
		this.body.velocity.y = -250;
	this.body.velocity.x = 800 * mod * strength;
	// Attack in self defense;
	if (this.state == PATROL) {
		this.setState(CHARGING);
		this.target = bouncer;
	}
}

Apricot.prototype.movementNormal = function() {
	if (this.sightDelay <= 0) {
		if (this.playerIsClose()) {
			this.setState(CHARGING);
			this.target = this.player;
			return;
		}
	} else
		this.sightDelay -= game.time.elapsed/1000;
	
	
	if (this.isStanding()) {
		this.body.maxVelocity.x = this.walkSpeed;
	}
	
	// If walking into a wall for 1 second, turn around
	if (this.body.x == this.body.prev.x) {
		if (this.wallTimer > 0) {
			this.wallTimer -= game.time.elapsed/1000;
		} else {
			this.wallTimer = 0.1;
			if (this.scale.x == 1)
				this.changeDirection('left');
			else
				this.changeDirection('right');
		}
	} else {
		this.wallTimer = 0.1;
	}
	
	var rightPos = this.x + this.width/2;
	var leftPos = rightPos - this.spriteEdge.width;
	if (this.edgeDelay > 0) {
		this.edgeDelay -= game.time.elapsed / 1000;
	} else {
		if (!this.spriteEdge.onPlatform && this.isStanding()) {
			this.edgeDelay = 0.25;
			this.spriteEdge.y = this.y + this.height/2-1;
			if (this.body.velocity.x <= 0) {
				this.changeDirection("right");
			} else if (this.body.velocity.x > 0) {
				this.changeDirection("left");
			}
		} else {
			if (this.body.velocity.x > 0 && this.body.velocity.x < this.walkSpeed-1) {
				this.spriteEdge.x = rightPos;
			} else if (this.body.velocity.x < 0 && this.body.velocity.x > -this.walkSpeed+1) {
				this.spriteEdge.x = leftPos;
			}
		}
	}
	
	if (!this.isStanding()) {
		this.resetSpriteEdgePos();
	}
	
	this.spriteEdge.body.velocity.x = this.body.velocity.x;
	this.spriteEdge.onPlatform = false;
}

Apricot.prototype.setState = function(newState) {
	this.state = newState;
	switch (newState) {
		case PATROL:
			this.animations.play('walk');
			this.spriteEdge.enable = true;
			this.body.bounce.x = 0;
			this.sightDelay = 0.5;
			return;
		case CHARGING:
			this.animations.play('roll');
			this.spriteEdge.enable = false;
			this.direction = null;
			this.chargeTimer = 0;
			return;
		case ROLLING:
			this.animations.play('roll');
			this.spriteEdge.enable = false;
			this.body.bounce.x = 1;
			this.body.maxVelocity.x = 600;
			this.bounced = false;
			if (this.direction == 'left')
				this.body.velocity.x = -600;
			else
				this.body.velocity.x = 600;
			return;
	}
}

Apricot.prototype.movementCharging = function() {
	/* Charge for at least one second. Then, if the player's height becomes close to apricot's height, roll. If that doesn't happen,
	then charge for another 1.5 seconds and then roll.(times may change) */
	if (this.body.velocity.x > 20) {
		this.body.acceleration.x = -400;
	} else if (this.body.velocity.x < -20) {
		this.body.acceleration.x = 400;
	} else {
		this.body.velocity.x = 0;
		this.body.acceleration.x = 0;
	}
	
	// Attack in the direction of this.target
	if (!this.direction && this.target) {
		if (this.target.x < this.x)
			this.direction = 'left';
		else
			this.direction = 'right';
	}
	
	if (this.chargeTimer > 2) {
		if (this.chargeTimer > 3.5 ||
		(this.target.y > this.y-64 && this.target.y < this.y+64)) {
			this.setState(ROLLING);
		}
	}
	this.chargeTimer += game.time.elapsed/1000;
}

Apricot.prototype.movementRolling = function() {
	if (this.isStanding())
		this.body.drag.x = 200;
	else
		this.body.drag.x = 0;
	
	if (this.bounced || Math.abs(this.body.velocity.x) <= 10)
		this.setState(PATROL);
}

Apricot.prototype.playerIsClose = function() {
	var rightPos = this.x + this.width/2;
	var leftPos = this.x - this.width/2;
	var xSight = 380;
	var ySight = 210;
	// Check y first
	if (this.player.y < this.y+this.height/2 &&
	this.player.y > this.y+this.height/2 - 210) {
		if (this.scale.x == 1) {
			// Facing right
			if (this.player.x > rightPos && this.player.x < rightPos + xSight)
				return true;
		} else {
			// Facing left
			if (this.player.x+this.player.width/2 < leftPos && this.player.x > leftPos - xSight)
				return true;
		}
	}
}

Apricot.prototype.resetSpriteEdgePos = function() {
	var rightPos = this.x + this.width/2;
	var leftPos = rightPos - this.spriteEdge.width;
	if (this.scale.x == 1) {
		this.spriteEdge.x = rightPos;
	} else {
		this.spriteEdge.x = leftPos;
	}
	this.spriteEdge.y = this.y + this.height/2-1;
	this.edgeDelay = 0.25;
}

Apricot.prototype.changeDirection = function(dir) {
	this.sightDelay = 0.5;
	var rightPos = this.x + this.width/2;
	var leftPos = rightPos - this.spriteEdge.width;
	if (dir == "right") {
		this.scale.x = 1;
		this.body.acceleration.x = 400;
		this.spriteEdge.x = rightPos;
	} else {
		this.scale.x = -1;
		this.body.acceleration.x = -400;
		this.spriteEdge.x = leftPos;
	}
}

Apricot.prototype.isStanding = function() {
	if (this.body.touching.down || this.body.blocked.down)
		return true;
}

Apricot.prototype.reset_ = function() {
	this.wallTimer = 0.1;
	this.body.reset(this.x, this.y);
	this.setState(PATROL);
	if (this.originalRev)
		this.changeDirection("left");
	else
		this.changeDirection("right");
}
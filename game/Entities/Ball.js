Ball.prototype = Object.create(Throwable.prototype);
Ball.prototype.constructor = Ball;

function Ball(x, y) {
	Throwable.call(this, x, y, 'ball');
	this.init();
	this.name = 'Ball';
	this.body.bounce.x = 0.75;
	this.body.bounce.y = 0.75;
	this.throwSpeedY = 400;
	this.crateFlip = true;
	this.holdOffsetX = 15;
	this.canRotate = true;
}

Ball.prototype.update = function() {
	if (this.body.blocked.down || this.body.touching.down) {
		this.body.drag.x = 100;
	} else {
		this.body.drag.x = 0;
	}
	
	if (this.canRotate)
		this.rotation += this.body.velocity.x / 3000;
}

Ball.prototype.pickedUp = function() {
	this.rotation = 0;
	this.canRotate = false;
}

Ball.prototype.justThrown = function() {
	this.canRotate = true;
}
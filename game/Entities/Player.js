function Player(x, y) {
	Phaser.Sprite.call(this, game, x, y, 'gumpy');
	
	// Movement properties
	this.standing = false;
	this.walkSpeed = 220;
	
	// Movement (constants)
	this.firstJumpSpeed = 350;
	this.secondJumpSpeed = 350;
	
	// Movement (changes)
	this.timerHighJump = 0.7;
	this.timerDashCooldown = 0.25;
	this.timerDashDuration = 0.5;
	this.enableBounceDelay = 0.5;
	// period: longest time player will sprout after hitting the key
	// if can't sprout before the period runs out the player will have
	// to press the key again
	this.timerSproutPeriod = 1.5;
	this.timerAirJump = 0.09;
	this.doHighJump = true;
	this.holdingJump = false;
	this.dashing = false;
	this.rotateLeft = false;
	this.restoreHeight = false;
	this.canSprout = true;
	
	this.needsPosition = true;
	
	this.resetPoint = {
		x: 0, y: 0,
		set: function(nx, ny) {
			this.x = nx;
			this.y = ny;
		}
	};
}

Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;

Player.prototype.initialize = function() {
	// Create sprite
	this.name = 'Player';
	
	game.physics.arcade.enable(this);
	this.resetGravity();
	this.body.collideWorldBounds = true;
	//this.body.maxVelocity.y = 1.5*this.secondJumpSpeed;
	this.width = 32;
	this.height = 64;
	this.body.height = 58;
	
	// Secondary collision for sprouting
	this.sproutSprite = game.add.sprite();
	game.physics.arcade.enable(this.sproutSprite);
	//this.sproutSprite.collideWorldBounds = true;
	this.sproutSprite.width = 32;
	this.sproutSprite.height = 20;
	
	// For a wider grab collision
	this.grabSprite = game.add.sprite();
	game.physics.arcade.enable(this.grabSprite);
	this.grabSprite.width = 42;
	this.grabSprite.height = 74;
	
	// So the sprite is flipped from the centre
	this.anchor.setTo(0.5, 0.625);
	this.sproutSprite.anchor.setTo(0.5, 0.625);
	this.grabSprite.anchor.setTo(0.5, 0.625);
	
	// Player animations
	this.animations.add('idle', [1], 1, true);
	this.animations.add('walk', [3,4,5,6,7], 8, true);
	this.animations.add('jump', [2], 1, true);
	this.animations.add('dash', [0], 1, true);
		
	this.animations.play('idle');
	
	// Timers
	this.resetTimerHighJump();
	this.pickupBanTimer = 0;
	
	// TURN THIS OFF //
	this.inputEnabled = true;
	this.input.enableDrag();
	
	this.eomX = 0;
};

Player.prototype.update = function()  {
	this.setStanding(this.body.blocked.down || this.body.touching.down);
	this.whileInAir();
	
	if (this.body.velocity.y > 0)
		this.body.maxVelocity.y = 800;
	else
		this.body.maxVelocity.y = 6000;

	// Update timers and stuff
	var elapsed = game.time.elapsed / 1000;
	
	if (this.timerDashCooldown > 0)
		this.timerDashCooldown -= elapsed;
	if (this.pickupSameItemCooldown > 0)
		this.pickupSameItemCooldown -= elapsed;
	if (this.pickupBanTimer > 0)
		this.pickupBanTimer -= elapsed;
	if (this.timerSproutPeriod > 0) {
		this.timerSproutPeriod -= elapsed;
		if (this.timerSproutPeriod <= 0)
			this.trySprout = false;
	}
	
	var roomTiles = tiles.getTiles(this.x-200,this.y-200,
	400,400,true,true);
	var sx = this.sproutSprite.body.x; var sy = this.sproutSprite.y;
	var sh = this.sproutSprite.height; var sw = this.sproutSprite.width;
	var canSprout = true;
	roomTiles.forEach(function(tile) {
		if (tile.intersects(sx, sy-sh, sx+sw, sy)) {
			if (!tile.properties.jt) {
				canSprout = false;
				return;
			}
		}
	});
	this.canSprout = canSprout;
	
	// Dash timing
	if (this.dashing) {
			this.timerDashDuration -= elapsed;
			this.rotation += this.body.velocity.x / 3000;
			// Flip based on velocity
			/*if (this.body.velocity.x > 0)
				this.scale.x = 1;
			else if (this.body.velocity.x < 0)
				this.scale.x = -1;*/
		if (this.trySprout && this.canSprout)
			this.sprout(true);
	}
		
	if (!this.standing) {
		this.timerAirJump -= elapsed;
	}
	
	if (this.restoreHeight) {
		if (this.body.height < 58) {
			this.body.height += 2;
			this.body.width = 32;
		} else {
			this.restoreHeight = false;
		}
	}
	
	if (this.heldItem)
		this.heldItem.update();
	
	if (this.movingToNextRoom) {
		this.moveToNextRoom();
	}
	
	this.setPlayerAnimation();
};

Player.prototype.sprout = function(hop=false) {
	this.dashHop = true;
	this.dashing = false;
	this.resetTimerDashCooldown();
	this.body.bounce.set(0, 0);
	this.rotation = 0;
	this.restoreHeight = true;
	if (hop) {
		this.body.velocity.y = -200;
		this.resetTimerAirJump();
	}
	this.resetXAnchor();
	this.trySprout = false;
	this.canDash = false;
}

Player.prototype.bounceBack = function(left, strength) {
	var mod = 1;
	if (left)
		mod = -1;
	if (strength >= 1)
		this.dash();
	this.body.velocity.y = -this.walkSpeed;
	this.body.velocity.x = this.walkSpeed * mod * strength;
}

Player.prototype.setStanding = function(stand) {
	this.standing = stand;
	if (this.standing) {
		this.resetTimerAirJump();
		this.dashHop = false;
		this.canDash = true;
	}
};

Player.prototype.setSproutSpritePos = function() {
	// Sprout sprite position
	//game.physics.arcade.moveToObject(this.sproutSprite, this.sprite, 10, 0.1);
	//this.sproutSprite.position.setTo(this.x+velX, this.y-20+velY);
	//this.canSprout = true;
	
	// Extend the sprout checker sprite if player is moving quick in a direction
	this.sproutSprite.body.width = 50;
	if (this.body.velocity.x > 300) {
		var mod = 1;
	} else if (this.body.velocity.x < -300) {
		var mod = -1;
	} else {
		var mod = 0;
		this.sproutSprite.body.width = 32;
	}
		
	this.sproutSprite.x = this.x + (30*mod);
	this.sproutSprite.y = this.y-30;
};

Player.prototype.setGrabSpritePos = function() {
	this.grabSprite.body.width = 50;
	if (this.body.velocity.x > 100) {
		var modX = 1;
	} else if (this.body.velocity.x < -100) {
		var modX = -1;
	} else {
		var modX = 0;
	this.grabSprite.body.width = 42;
	}
	
	this.grabSprite.body.height = 80;
	if (this.body.velocity.y > 100) {
		var modY = 1;
	} else if (this.body.velocity.y < -100) {
		var modY = -1;
	} else {
		var modY = 0;
		this.grabSprite.body.height = 74;
	}
	this.grabSprite.x = this.x + (30*modX);
	this.grabSprite.y = this.y + (30*modY);
	
}

Player.prototype.move = function(moveType) {
	// NOTE: MaxVelocity works with negative velocities too
	
	if (this.body.moves == false || this.ignoreInput)
		return;
	
	this.tryGrab = false;
	switch (moveType) {
		case MovementEnum.LEFT:
			if (!this.dashing) {
				this.body.acceleration.x = -2250;
				this.body.maxVelocity.x = this.walkSpeed;
				this.scale.x = -1;
			} else {
				// Dash deceleration
				this.body.acceleration.x = -220;
			}
			break;
		case MovementEnum.RIGHT:
			if (!this.dashing) {
				this.body.acceleration.x = 2250;
				this.body.maxVelocity.x = this.walkSpeed;
				this.scale.x = 1;
			} else {
				// Dash deceleration
				this.body.acceleration.x = 220;
			}
			break;
		case MovementEnum.UP:
			if (this.dashing && this.timerDashDuration <= 0) {
				this.trySprout = true;
				this.resetTimerSproutPeriod();
			}
			break;
		case MovementEnum.DOWN:
			this.dash(false);
			break;
		case MovementEnum.JUMP:
			if (!this.dashing)
				this.jump();
			else
				this.dash(true);
			break;
		case MovementEnum.GRAB:
			this.grab();
			break;
		default:
			if (!this.dashing)
				this.decelerate();
			else
				this.body.acceleration.x = 0;
		}
};

Player.prototype.startMoveToNextRoom = function(doorIsVertical, roomRect) {
	this.doorIsVertical = doorIsVertical;
	this.roomRect = roomRect;
	if (doorIsVertical) {
		this.ignoreInput = true;
		this.oldVelocity = this.body.velocity.x;
	} else
		this.oldVelocity = this.body.velocity.y;
	this.movingToNextRoom = true;
	this.roomTransitionTimeout = 5;
	this.body.acceleration.x = 0;
}

Player.prototype.moveToNextRoom = function() {
	/* Move to the next room.
		As a safeguard, if the player is not in the next room
		after 5 seconds, teleport them to that room's start position.*/
	var done = false;
	if (this.doorIsVertical) {
		if (this.x < this.roomRect.x + 32) {
			// Move right
			this.body.velocity.x = 60;
			if (this.body.blocked.right && this.body.velocity.y == 0)
				this.dash(false);
		} else if (this.x+this.width > this.roomRect.x+this.roomRect.width - 32) {
			// Move left
			this.body.velocity.x = -60;
			if (this.body.blocked.left && this.body.velocity.y == 0)
				this.dash(false);
		} else
			done = true;
	} else {
		if (this.body.velocity.x > 45)
			this.body.velocity.x = 45;
		if (this.body.velocity.x < -45)
			this.body.velocity.x = -45;
		if (this.y < this.roomRect.y + 32) {
			// Fall
			this.body.velocity.y = 70;
		} else if (this.y+this.height > this.roomRect.y + this.roomRect.height) {
			// Rise
			this.body.velocity.y = -70;
		} else
			done = true;
	}
	
	this.roomTransitionTimeout -= game.time.elapsed / 1000;
	if (!done && this.roomTransitionTimeout <= 0) {
		this.position.set(this.resetPoint.x, this.resetPoint.y);
		done = true;
	}
	
	if (done) {
		this.ignoreInput = false;
		this.movingToNextRoom = false;
		if (this.doorIsVertical)
			this.body.velocity.x = this.oldVelocity;
		else
			this.body.velocity.y = this.oldVelocity;
		lastDoor.close_();
	}
}

Player.prototype.endOfMap = function(firstCall) {
	if (firstCall) {
		this.ignoreInput = true;
		this.eomX = this.x;
		this.body.gravity.y = 0;
		this.body.acceleration.x = 0;
	}
	
	if (this.body.velocity.x >= 0) {
		this.body.velocity.x = 60
		if (this.x > this.eomX + 128) {
			this.readyForNextMap = true;
		}
	} else {
		this.body.velocity.x = -60;
		if (this.x < this.eomX - 128) {
			this.readyForNextMap = true;
		}
	}
	
	if (this.readyForNextMap) {
		this.body.acceleration.x = 0;
		this.eomX = 0;
		this.body.velocity.x = 0;
		this.resetGravity();
		this.ignoreInput = false;
	}
	
}

Player.prototype.dash = function(hop) {
	if (!this.canDash)
		return;
	
	if ((!this.dashing || this.standing) && hop)
			this.body.velocity.y = -250;
	
	// Keep constant velocity until dash is over
	if (!this.dashing && this.timerDashCooldown <= 0) {
		// Throw current item
		this.dashing = true;
		this.resetTimerDashDuration();
		
		this.setToBallBounce();
		this.body.acceleration.x = 0;
		this.body.maxVelocity.x = this.walkSpeed*2;
		this.body.velocity.x = 1.8 * this.body.velocity.x;
		//this.body.velocity.y -= 250;
		//this.body.maxVelocity.y = this.firstJumpSpeed*1.1;
		this.restoreHeight = false;
		
		this.body.height = 29;
		this.body.width = 28;
		this.anchor.x = 0.4375;

	}
};

Player.prototype.grab = function() {
	if (this.heldItem) {
		this.throwItem();
	} else {
		var highestPriority;
		// Player overlap with throwables	
		for (i = 0; i < throwables.length; ++i) {
			if (game.physics.arcade.intersects(
			this.body, throwables.getAt(i).body)) {
				if (throwables.getAt(i).destroyed || (throwables.getAt(i) === this.lastHeldItem && this.pickupSameItemCooldown > 0))
					continue;
				if (highestPriority)
					highestPriority = getHigherPriority(highestPriority, throwables.getAt(i));
				else
					highestPriority = throwables.getAt(i);
			} else if (game.physics.arcade.intersects(
				this.grabSprite.body, throwables.getAt(i).body)) {
				if (throwables.getAt(i).destroyed || (throwables.getAt(i) === this.lastHeldItem && this.pickupSameItemCooldown > 0))
					continue;
				if (highestPriority)
					highestPriority = getHigherPriority(highestPriority, throwables.getAt(i));
				else
					highestPriority = throwables.getAt(i);
			}
		}
		
		if (highestPriority)
			this.pickup(highestPriority);

	}
}

Player.prototype.pickup = function(throwable) {
	if (this.pickupBanTimer > 0)
		return;
	this.heldItem = throwable;
	this.addChild(throwable);
	throwable.position.set(throwable.holdOffsetX,throwable.holdOffsetY);
	throwable.body.moves = false;
	
	// This if statement makes it so child is NOT
	// flipped when picked up
	if (throwable.crateFlip && this.scale.x == -1)
		throwable.scale.x *= -1;
	else
		throwable.scale.x = 1;
	throwable.pickedUp();
}

Player.prototype.throwItem = function() {
	var gumpXVel = this.body.velocity.x;
	this.removeChild(this.heldItem);
	if (cursors.up.isDown || wasd.up.isDown) {
		this.heldItem.body.velocity.y = -this.heldItem.throwSpeedY - 200;
		this.heldItem.body.velocity.x = gumpXVel*0.8;
		var throwDirection = 'up';
	} else if ((cursors.down.isDown || wasd.down.isDown) && Math.abs(gumpXVel) < this.walkSpeed) {
		this.heldItem.body.velocity.y = 50;
		this.heldItem.body.velocity.x = 0;
		var throwDirection = 'down';
	} else {
		this.heldItem.body.velocity.y = -this.heldItem.throwSpeedY;
		this.heldItem.body.velocity.x = gumpXVel * 1.3;
		var throwDirection = 'side';
		if (!this.dashing) {
			if (this.scale.x == 1) {
				// If JUST turned around, and is slowing down.
				// While dashing, ignore this, just throw in direction of movement
				if (gumpXVel < 0)
					this.heldItem.body.velocity.x = this.heldItem.throwSpeedX;
				else
					this.heldItem.body.velocity.x += this.heldItem.throwSpeedX;
				} else {
					if (gumpXVel > 0)
						this.heldItem.body.velocity.x = -this.heldItem.throwSpeedX;
					else
						this.heldItem.body.velocity.x -= this.heldItem.throwSpeedX;
				}
			}
		}
		
	// because dashing makes you smaller
	var y_off = 0;
	if (this.dashing)
		y_off = -8

	if (this.scale.x == 1)
		this.heldItem.position.set(this.x-this.heldItem.throwSafetyNet, this.y+y_off);
	else
		this.heldItem.position.set(this.x+this.heldItem.throwSafetyNet, this.y+y_off);

	if (this.scale.x == -1)
		this.heldItem.scale.x *= -1;
		
	this.heldItem.justThrown(throwDirection);
	this.heldItem.body.moves = true;
	throwables.add(this.heldItem);
	this.lastHeldItem = this.heldItem;
	this.resetPickupSameItemCooldown();

	this.heldItem = null;
}

Player.prototype.decelerate = function() {	
	// To decelerate, modifier = -1
	if (this.body.velocity.x > 50) {
		var modifier = -1;
	} else if (this.body.velocity.x < -50) {
		var modifier = 1;
	}
	
	if (this.standing)
		this.body.acceleration.x = 1000 * modifier;
	else
		this.body.acceleration.x = 750 * modifier;
	
	if (this.body.velocity.x <= 50 && this.body.velocity.x >= -50) {
		this.body.acceleration.x = 0;
		this.body.velocity.x = 0;
	}

};

Player.prototype.jump = function() {
	if (!this.dashHop && (this.standing || this.timerAirJump > 0)) {
		// Only happens once, for start of jump
		this.body.velocity.y = -this.firstJumpSpeed;
		this.doHighJump = true;
		this.resetTimerHighJump();
	}
};

Player.prototype.whileInAir = function() {
	// Can't carry out high jump if already falling (hit ceiling)
	if (!this.holdingJump || this.body.velocity.y > 0)
		this.doHighJump = false;
	
	if (this.doHighJump) {
		if (this.timerHighJump <= 0) {
			// Extra jump boost
			this.body.velocity.y = -this.secondJumpSpeed;
			this.doHighJump = false;
		} else {
			this.timerHighJump -= game.time.elapsed / 1000;
		}
	}
};

Player.prototype.setPlayerAnimation = function() {
	if (this.dashing) {
		this.animations.play('dash');
	} else if (this.standing || this.eomX) {
		if (this.body.velocity.x != 0) {
			this.animations.play('walk');
		} else {
			this.animations.play('idle');
		}
	} else {
		this.animations.play('jump');
	}
};

Player.prototype.getName = function() {
	return "Player";
}

/*--------------------------------------------------------------------
	Reset variables
--------------------------------------------------------------------*/
Player.prototype.resetTimerHighJump = function() {
	this.timerHighJump = 0.12;
};

Player.prototype.resetTimerDashDuration = function() {
	this.timerDashDuration = 0.5;
};

Player.prototype.resetTimerDashCooldown = function() {
	this.timerDashCooldown = 0.25;
};

Player.prototype.resetTimerSproutPeriod = function() {
	this.timerSproutPeriod = 1.5;
}

Player.prototype.resetTimerAirJump = function() {
	this.timerAirJump = 0.09
}

Player.prototype.resetBodyHeight = function() {
	//this.body.height = 56;
	//this.body.center.set(16,32); 
}

Player.prototype.resetXAnchor = function() {
	this.anchor.x = 0.5;
}

Player.prototype.resetPickupSameItemCooldown = function() {
	this.pickupSameItemCooldown = 0.5;
}

Player.prototype.setToBallBounce = function() {
	this.body.bounce.x = 0.75;
}

Player.prototype.resetGravity = function() {
	this.body.gravity.y = 850;
}

Player.prototype.reset_ = function() {
	// Drop item
	this.dropItem();
	this.sprout();
	this.body.reset(this.x, this.y);
}

// Force a drop
Player.prototype.dropItem = function(pickupBanTimer) {
	this.pickupBanTimer = pickupBanTimer;
	if (this.heldItem != null) {
		this.removeChild(this.heldItem);
		this.heldItem.body.moves = true;
		this.heldItem.justThrown('side');
		this.heldItem.position.set(this.x, this.y);
		throwables.add(this.heldItem);
		this.lastHeldItem = null;
		this.heldItem = null;
	}	
}
/*--------------------------------------------------------------------

--------------------------------------------------------------------*/
// Collision
/*
Player.prototype.processCollision = function(other) {
	if (other.getName == 'Crate') {
		if (this.sprite.y >= other.y + other.height)
			return true;
		else if (this.sprite.y+this.sprite.height <= other.y)
			return true;
		return false;
	}
}*/


// Variable descriptions
/* firstJumpSpeed: the jump is two parts. Tapping jump just does a small jump using firstJumpSpeed.
secondJumpSpeed: if the jump button is held, a second boost is given midjump (should not be noticable)
timerHighJump: the amount of time the jump button must be held before the second jump boost kicks in
doHighJump: set to true at the start of a jump. becomes false if jump is not held
holdingJump: immediately set to true/false depending on whether the player is holding jump 
timerAirJump: the time the player is allowed to jump after falling off a platform 
*/
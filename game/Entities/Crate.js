Crate.prototype = Object.create(Throwable.prototype);
Crate.prototype.constructor = Crate;

function Crate(data) {
	Throwable.call(this, data.x, data.y, 'crate');
	this.init();
	this.name = 'Crate';
	this.body.bounce.x = 0.2;
	this.body.bounce.y = 0.15;
	this.crateFlip = true;
	this.holdOffsetX = 15;
}

Crate.prototype.pickedUp = function() {
	this.rotation = -0.1;
}

Crate.prototype.update = function() {
	if (this.body.blocked.down || this.body.touching.down) {
		this.body.drag.x = 700;
	} else {
		this.body.drag.x = 0;
	}
}

Crate.prototype.justThrown = function() {
	this.rotation = 0;
}
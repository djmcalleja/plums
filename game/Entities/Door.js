Door.prototype = Object.create(Phaser.Sprite.prototype);
Door.prototype.constructor = Door;

function Door(data) {
	Phaser.Sprite.call(this, game, data.x, data.y, 'door');
	var x = data.x; var y = data.y;
	game.physics.arcade.enable(this);
	this.setSize(data.w, data.h);
	
	this.name = 'Door';
	this.body.immovable = true;
	this.body.allowGravity = false;
	this.rev = data.rev;
	
	this.resetPoint = {
		x: x, y: y,
		set: function(nx, ny) {
			this.x = nx;
			this.y = ny;
		}
	};
	
	this.opening = false;
	this.closed_ = true;
	this.chunkDelay = 0.5;
	
	/* A door should only be killed when the player goes to the room
	after the room the door was in. When the player goes to the next room
	while a door is 'old' it can be removed.*/
	this.oldDoor = false;
}

Door.prototype.setSize = function(w, h) {
	this.width = w;
	this.height = h;
	this.body.width = w;
	this.body.height = h;
}

Door.prototype.update = function() {
	var vertDoor = (this.width == 64);
	
	//  0: vibrate	  1: chunk	  2: accelerate
	var elapsed = game.time.elapsed / 1000;
	
	if (this.opening) {
		var revMod = 1;
		if (this.rev)
			revMod = -1;
		
		if (vertDoor) {
			// Vertical door
			switch (this.openType) {
				case 0:
					if (this.chunkDelay <= 0) {
						this.y -= 16 * revMod;
						this.chunkDelay = 0.25;
					} else
						this.chunkDelay -= elapsed;
					break;
				case 1:
					this.doorSpeedsSpeed += 0.001;
					this.doorSpeed += this.doorSpeedsSpeed/8;
					this.y -= this.doorSpeed * revMod;
					break;
			}
			if (revMod)
				if (this.y >= this.resetPoint.y + this.height) {
					this.y = this.resetPoint.y + this.height;
					this.movementDone();
				}
			else
				if (this.y <= this.resetPoint.y - this.height) {
					this.y = this.resetPoint.y - this.height;
					this.movementDone();
				}
		} else {
			// Horizontal door
			switch (this.openType) {
				case 0:
					if (this.chunkDelay <= 0) {
						this.x -= 16 * revMod;
						this.chunkDelay = 0.25;
					} else
						this.chunkDelay -= elapsed;
					break;
				case 1:
					this.doorSpeedsSpeed += 0.001;
					this.doorSpeed += this.doorSpeedsSpeed/8;
					this.x -= this.doorSpeed * revMod;
					break;
			}
			if (revMod)
				if (this.x >= this.resetPoint.x + this.width) {
					this.x = this.resetPoint.x + this.width;
					this.movementDone();
				}
			else	
				if (this.x <= this.resetPoint.x - this.width) {
					this.x = this.resetPoint.x - this.width;
					this.movementDone();
				}
		}
	} else if (this.closing) {
		var done = false;
		if (this.width==64) {
			if (!this.rev) {
				if (this.y < this.resetPoint.y) {
					this.y += 4;
				} else {
					done = true;
				}
			} else {
				if (this.y > this.resetPoint.y) {
					this.y -= 4;
				} else {
					done = true;
				}
			}
		} else {
			if (!this.rev) {
				if (this.x < this.resetPoint.x) {
					this.x += 4;
				} else {
					done = true;
				}
			} else {
				if (this.x > this.resetPoint.x) {
					this.x -= 4;
				} else {
					done = true;
				}
			}
		}
		this.setBodyPosition();
		if (done) {
			this.x = this.resetPoint.x;
			this.y = this.resetPoint.y;	
			this.closing = false;
		}
	}
}

Door.prototype.setBodyPosition = function() {
	// Body position does not move.
	this.body.offset.x = this.resetPoint.x - this.x;
	this.body.offset.y = this.resetPoint.y - this.y;
}

Door.prototype.setOpen = function(opening) {
	// Quick open
	this.movementDone();
	
	
	/*this.opening = opening;
	pausePhysics(true);
	this.openType = Math.floor(Math.random() * 2);
	this.doorSpeed = 0;
	this.doorSpeedsSpeed = 0;*/
}

Door.prototype.movementDone = function() {
	// specifically after door OPENS and is hidden by a wall
	this.opening = false;
	this.closed_ = false;
	this.setBodyPosition();
	lookAtDoor(true);
	this.alpha = 0;
}

Door.prototype.isVertical = function() {
	return (this.width==64);
}

Door.prototype.reset_ = function(data, fullReset) {
	// Close me
	this.setHasKey(false);
	if (fullReset) {
		this.body.enable = true;
		this.closed_ = true;
		this.closing = false;
		this.opening = false;
		this.x = data.x;
		this.y = data.y;
		this.setSize(data.w, data.h);
		this.rev = data.rev;
		this.oldDoor = false;
	} else {
		this.close_();
	}
	if (!this.oldDoor)
		this.alpha = 1;
}

Door.prototype.setHasKey = function(hasKey) {
	this.hasKey = hasKey;
}

Door.prototype.close_ = function() {
	this.closing = true;
	this.opening = false;
	this.closed_ = true;
}
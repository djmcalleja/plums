Spike.prototype = Object.create(Phaser.TileSprite.prototype);
Spike.prototype.constructor = Spike;
DEF = 0;
QUICK = 1;

function Spike(data) {
	Phaser.TileSprite.call(this, game, data.x, data.y, data.w, data.h, 'spikes blue');
	game.physics.arcade.enable(this);
	var x = data.x;
	var y = data.y;
	this.name = 'Spike';
	this.body.allowGravity = false;
	this.body.immovable = true;
	this.body.height = data.h-16;
	this.body.width = data.w-32;
	this.body.position.x += 16;
	
	this.delay = 0;
	if (data.delay != 0)
		this.delay = data.delay*1000.0;
	
	this.anchor.set(0.5, 0.5);
	if (data.flippedY) {
		negmod = -1;
		this.scale.y = -1;
		this.body.offset.y = -4;
		this.position.y -= 4;
	} else {
		this.body.offset.y = 4;
		this.position.y += 4;
	}
		this.position.y -= 8;
		
	this.resetPoint = {
		x: x, y: y,
		set: function(nx, ny) {
			this.x = nx;
			this.y = ny;
		}
	};

	if (data.inTime && data.outTime) {
		this.moveType = QUICK;
		this.inTime = data.inTime;
		this.outTime = data.outTime;
	} else {
		this.moveType = DEF;
	}
}

Spike.prototype.update = function() {
	var negmod = this.scale.y;
	if (this.started) {
		if (this.scale.y == -1) {
			if (this.y < this.resetPoint.y-21) {
				this.y = this.resetPoint.y-21;
				this.body.velocity.y = 0;
			} else if (this.y > this.resetPoint.y-2) {
				this.y = this.resetPoint.y-2;
				this.body.velocity.y = 0;
			}
		} else {
			if (this.y > this.resetPoint.y+21) {
				this.y = this.resetPoint.y+21;
				this.body.velocity.y = 0;
			} else if (this.y < this.resetPoint.y+2) {
				this.y = this.resetPoint.y+2;
				this.body.velocity.y = 0;
			}
		}
		
		if (this.tweenDelay > 0)
			this.tweenDelay -= game.time.elapsed / 1000;
		else if (this.nextTween) {
			this.retracted = false;
			this.nextTween.start();
			this.nextTween = undefined;
		}
		return;
	}
	
	switch (this.moveType) {
		case DEF:
			this.defaultMovement();
			break;
		case QUICK:
			this.quickMovement();
			break;
	}
	this.started = true;
}

Spike.prototype.defaultMovement = function() {
		negmod = this.scale.y;
		g = this.position.y;
		// Random speed?
		//speed = Math.floor( Math.random() * 400 ) + 300;
		this.movementTween = game.add.tween(this).to( { y:g+(8*negmod) }, 300, Phaser.Easing.Quadratic.InOut, true, 0, Number.MAX_VALUE, true);
}

Spike.prototype.quickMovement = function() {
	negmod = this.scale.y;
	g = this.position.y;
	// go in
	this.inTween = game.add.tween(this.body.velocity).to( 
	{ y:300*negmod }, 
	50,
	null,
	true,
	this.delay
	);
	
	this.movementTween = game.add.tween(this.body.velocity).to( 
	{ y:-300*negmod }, 
	50,
	null,
	false
	);
		
	this.inTween.onComplete.add(this.startOutTween, this, undefined);
	this.movementTween.onComplete.add(this.startInTween, this, undefined);
}

Spike.prototype.stopTweens = function() {
	if (this.inTween)
		this.inTween.stop();
	if (this.outTween)
		this.outTween.stop();
	if (this.movementTween)
		this.movementTween.stop();
}

Spike.prototype.waveMovement = function() {
	
}

Spike.prototype.startOutTween = function(out) {
	this.retracted = true;
	this.nextTween = this.movementTween;
	this.tweenDelay = this.inTime;
}

Spike.prototype.startInTween = function(out) {
	this.nextTween = this.inTween;
	this.inTween.delay(0);
	this.tweenDelay = this.outTime;
}

Spike.prototype.reset_ = function(data, poolReset) {
	// Spikes don't reset on a room restart
	if (!poolReset)
		return;
	
	// important that started = false
	this.started = false;
	this.body.enable = true;
	this.alpha = 1;
	this.x = data.x;
	this.y = data.y;
	this.width = data.w;
	this.height = data.h;
	this.body.height = data.h-16;
	this.body.width = data.w-32;
	this.body.velocity.y = 0;
	this.retracted = false;
	this.nextTween = undefined;
		
	if (data.delay != 0)
		this.delay = data.delay*1000.0;
	
	if (data.flippedY) {
		negmod = -1;
		this.scale.y = -1;
		this.body.offset.y = -4;
		this.position.y -= 4;
	} else {
		this.scale.y = 1;
		this.body.offset.y = 4;
		this.position.y += 4;
	}
		this.position.y -= 8;
		
	if (data.inTime && data.outTime) {
		this.moveType = QUICK;
		this.inTime = data.inTime;
		this.outTime = data.outTime;
	} else {
		this.moveType = DEF;
	}
}
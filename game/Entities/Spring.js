function Spring(x, y) {
	Phaser.Sprite.call(this, game, x, y, 'spring', 0);
}

Spring.prototype = Object.create(Phaser.Sprite.prototype);
Spring.prototype.constructor = Spring;

Spring.prototype.initialize = function(power) {
	game.physics.arcade.enable(this);
	this.name = 'Spring';
	this.body.allowGravity = false;
	this.body.immovable = true;
	//this.body.width = 24;
	//this.body.offset.x = 4;
	this.body.width = 30;
	this.body.offset.x = 1;
	this.animations.add('boing', [0,1,2,3,2,1,0], 30);
	this.power = power;
	this.animDelay = 0.2;
}

Spring.prototype.bounceObject = function(ob) {
	ob.body.velocity.y = -this.power;
	ob.y -= 1;
	if (this.animDelay <= 0)
		this.animations.play('boing');
}

Spring.prototype.update = function() {
	this.animDelay -= game.time.elapsed / 1000;
}

Spring.prototype.reset_ = function() {
}
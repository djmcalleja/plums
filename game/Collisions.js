function collisionEffect(sprite1, sprite2, onSecondRun=false) {
	if (sprite1.name == 'Player') {
		if (sprite2.name == 'Door') {
			if (sprite2 == currentDoor && sprite2.closed_ && sprite2.isVertical()) {
				if (sprite1.heldItem && sprite1.heldItem.name == 'Key' ){
					temp = sprite1.heldItem;
					sprite1.dropItem();
					temp.putInDoor();					
				}
			}
		}
		return;
	} else if (sprite1.name == 'Door') {
		if (sprite2.name == 'Key') {
			if (sprite1 == currentDoor)
				sprite2.putInDoor();
		}
	}
	// Run again but swap sprites
	if (!onSecondRun)
		collisionEffect(sprite2, sprite1, true);
}

function processCollision(sprite1, sprite2, onSecondRun=false) {
	if (sprite1.name == 'Player') {
		if (sprite2.name == 'Crate')
			return PlayerVSCrate(sprite1, sprite2);
		else if (sprite2.name == 'Door')
			if (sprite2.closed_)
				return true;
	} else if (sprite1.name == 'Door') {
		if (sprite2.name=='Crate'||sprite2.name=='Ball'||sprite2.name=='Key'||sprite2.name=='Apricot') {
			return true;
		}
	}
	// Run again but swap sprites
	if (!onSecondRun)
		return processCollision(sprite2, sprite1, true);
	// default collision is nothing
	return false;
}

function overlapEffect(sprite1, sprite2, onSecondRun=false) {
	if (sprite1.name == 'Player') {
		if (sprite2.name == 'Spike') {
			if (!sprite2.retracted)
				restartRoom(true);
				return;
		} else if (sprite2.name == 'Spring') {
			sprite2.bounceObject(sprite1);
		} else if (sprite2.name == 'Door') {
			if (!sprite2.closed_ && !sprite1.movingToNextRoom) {
				if (sprite2.isVertical()) {
					if (sprite1.body.y >= sprite2.body.y &&
						sprite1.y <= sprite2.body.y+sprite2.body.height) {
						nextRoom(sprite2);
						lastDoor = sprite2;
					}
				} else {
					if (sprite1.body.x >= sprite2.body.x-1 &&
						sprite1.body.x+sprite1.body.width <= sprite2.body.x+sprite2.body.width+1) {
						nextRoom(sprite2);
						lastDoor = sprite2;
					}
				}
			}
		} else if (sprite2.name == 'Apricot') {
			PlayerVSApricot(sprite1, sprite2);
		}
	}
			
	// Run again but swap sprites
	if (!onSecondRun)
		overlapEffect(sprite2, sprite1, true);
}

function processOverlap(sprite1, sprite2, onSecondRun=false) {
	if (sprite1.name == 'Player') {
		if (sprite2.name=='Spike'||sprite2.name=='Spring'||sprite2.name== 'Door'||sprite2.name=='Apricot')
			return true;
	}
	// Run again but swap sprites
	if (!onSecondRun)
		return processOverlap(sprite2, sprite1, true);
	// default overlap is nothing
	return false;
}

function tileCollisionEffect(entity, tile) {
	if (entity.name == 'ApricotEdge') {
		entity.onPlatform = true;
	}
}

function processTileCollision(entity, tile) {
	if (entity.name == 'Spike' || entity.name == 'Door')
		return false;
	
	ex = entity.body.x; ey = entity.body.y;
	ew = entity.body.width; eh = entity.body.height;
	if (tile.properties.jt) {
		if (ex+ew<=tile.left+12 || ex>=tile.right-12)
			return false;
		return true;
	}
	return true;
}

function PlayerVSCrate(player, crate) {
	/*if (player.y <= crate.y-crate.height) {
		return true;
	} else */
	return false;
	
	if (crate.y <= player.y-player.height+12) {
		return true;
	}
	return false;
}

function PlayerVSApricot(player, apricot) {
	apricot.body.maxVelocity.x = apricot.walkSpeed * 3;
	if (Math.abs(player.body.velocity.x) >= player.walkSpeed*2-40) {
		var apriStrength = 2;
		var playerStrength = 1;
	} else if (Math.abs(player.body.velocity.x) >= player.walkSpeed - 2){
		var apriStrength = 1;
		var playerStrength = 3;
	} else {
		var apriStrength = 1;
		var playerStrength = 5;
	}
	
	if (player.y+player.height < apricot.y+16) {
		playerStrength = 0;
	}
	
	if (player.x < apricot.x) {
		player.bounceBack(true, playerStrength);
		apricot.bounceBack(false, apriStrength, player);
	} else {
		player.bounceBack(false, playerStrength);
		apricot.bounceBack(true, apriStrength, player);
	}
}

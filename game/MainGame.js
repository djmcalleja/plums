var gameState = {
create: function() {
	
	game.physics.startSystem(Phaser.Physics.ARCADE);
	
	// World
	//game.world.setBounds(0,0, 1000, 1000);
	game.stage.setBackgroundColor('DFEE9B');
	
	entityInit();
	mapInit();
	
	game.world.sendToBack(tiles);
	game.world.sendToBack(tilesExtra);
	game.world.sendToBack(backEntities);
	game.world.sendToBack(roomBgs);
	game.world.sendToBack(bgSlab);
	
	// Camera
	game.camera.follow(player1, 0, 1,1);
	cameraDoorSprite = game.add.sprite();
	
	// Variable initial
	resetting = false;
	restartCooldown = 3;
	
	meBot = game.add.text(0, 0);
	meBot.addColor('#ffffff', 0);
	game.time.advancedTiming = true;
},

update: function() {
	// Collisions
	if (!resetting) {
		// Ground/platform collisions
		//player1.body.immovable = false;

		// Collides
		numEnt = entities.length;
		for (i = 0; i < numEnt; ++i) {
			for (k = i; k < numEnt; ++k) {
				game.physics.arcade.collide(entities[i], entities[k], collisionEffect, processCollision);
				game.physics.arcade.overlap(entities[i], entities[k], overlapEffect, processOverlap);
			}
			game.physics.arcade.collide(entities[i], tiles, tileCollisionEffect, processTileCollision);
			game.physics.arcade.collide(entities[i], tilesExtra, tileCollisionEffect, processTileCollision);
		}
		if (currentDoor && !currentDoor.opening)
			cameraDoorSprite.position.set(player1.x, player1.y);
		if (currentDoor && checkDoorCondition() && currentDoor.closed_ && !currentDoor.opening) {
			currentDoor.setOpen(true);
			lookAtDoor();
		}

	} else {
		resetObjectPositions();
	}
	
	if (transitionNextMap) {
		endOfMap();
	}
	
	// Putting this here made the slab not lag behind as much
	game.camera.update();
	
	if (restartCooldown > 0)
		restartCooldown -= game.time.elapsed / 1000;

	checkKeys(player1);

	mapUpdate();
	
	doDebug();
},

render: function() {
	player1.setSproutSpritePos();
	player1.setGrabSpritePos();
}
};

function pausePhysics(pause) {
	game.physics.arcade.isPaused = pause;
	player1.ignoreInput = pause;
}

function doDebug() {
	//game.debug.body(player1);
	//Debug 
	/*meBot.setText("j");
	meBot.x = player1.body.x;
	meBot.y = player1.body.y;
	//game.debug.body(player1);
    //game.debug.body(throwables);
	//game.debug.body(player1.sproutSprite);
	/*
	bumText.setText('CrateY = ' + (crate.sprite.body.y-crate.sprite.body.height) + '\nPlayerTop = ' + (player1.sprite.body.y+player1.sprite.body.height));
	bumText.x = player1.sprite.x-4;
	bumText.y = player1.sprite.y-player1.sprite.height;
	buttText.setText(crate.sprite.y + '\n' + (player1.sprite.y-player1.sprite.height));
    game.debug.body(crate.sprite);
	game.debug.body(player1.grabSprite);
	*/
	//meBot.setText(game.time.fps);
	/*meBot.x = player1.x;
	meBot.y = player1.y-player1.height;
	bum.setText(crate.sprite.body.y);
	bum.x = crate.sprite.body.x;
	bum.y = crate.sprite.body.y;
	*/
}

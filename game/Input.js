function setupKeys(player) {
	MovementEnum = {
		LEFT: 0,
		RIGHT: 1,
		UP: 2,
		DOWN: 3,
		JUMP: 4,
		GRAB: 5
	};

	// Keys
	cursors = game.input.keyboard.createCursorKeys();
	wasd = {up: game.input.keyboard.addKey(Phaser.KeyCode.W),
			down: game.input.keyboard.addKey(Phaser.KeyCode.S),
			left: game.input.keyboard.addKey(Phaser.KeyCode.A),
			right: game.input.keyboard.addKey(Phaser.KeyCode.D)};
	jumpKeys = {jump1: game.input.keyboard.addKey(Phaser.KeyCode.Z),
				jump2: game.input.keyboard.addKey(Phaser.KeyCode.K)};
	grabKeys = {grab1: game.input.keyboard.addKey(Phaser.KeyCode.X),
				grab2: game.input.keyboard.addKey(Phaser.KeyCode.L)};
	restartKeys = {restart1: game.input.keyboard.addKey(Phaser.KeyCode.R)};
				
	// Only needs to happen once			
	grabKeys.grab1.onDown.add(tellGrab, this);
	grabKeys.grab2.onDown.add(tellGrab, this);
	
	restartKeys.restart1.onDown.add(tellRestart);
	
	function tellRestart() {
		// Need this because onDown passes a Phaser.Key as first param
		restartRoom(false);
	}
	
	function tellGrab() {
		player.move(MovementEnum.GRAB);
	}			
}

var setGrab = false;	
function checkKeys(player) {
	/******************* TEMPORARY **************************/
	if (game.input.keyboard.isDown(Phaser.KeyCode.OPEN_BRACKET)) {
		currentDoor.setOpen(true);
	}
	if (game.input.keyboard.isDown(Phaser.KeyCode.CLOSED_BRACKET)) {
		player1.position.set(currentDoor.x+currentDoor.width/2, currentDoor.y-32);
	}
	/**********************************************************/
	
	if (wasd.right.isDown || cursors.right.isDown) {
		player.move(MovementEnum.RIGHT);
	} else if (wasd.left.isDown || cursors.left.isDown) {
		player.move(MovementEnum.LEFT);
	} else {
		// Don't move
		player.move();
	}
	
	if (wasd.up.isDown || cursors.up.isDown) {
		player.move(MovementEnum.UP);
	} else if (wasd.down.isDown || cursors.down.isDown) {
		player.move(MovementEnum.DOWN);
	}
	
	player.holdingJump = jumpKeys.jump1.isDown || jumpKeys.jump2.isDown;
	
	if ((jumpKeys.jump1.isDown && jumpKeys.jump1.duration < 200) ||
		(jumpKeys.jump2.isDown && jumpKeys.jump2.duration < 200)) {
			player.move(MovementEnum.JUMP);
		}
}
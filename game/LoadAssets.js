var loadState = {
preload: function() {
	/*
	for (i = 0; i < 5; ++i) {
		game.load.image('plum_walk' + i, 'assets/entities/BadPlum/walkingbody/plum_walk' + i + '.png');
	}
	game.load.image('plum_idle0', 'assets/entities/BadPlum/idle/plum_idle0.png');
	*/
	//game.load.spritesheet('plum', 'assets/entities/BadPlum/plum.png', 84, 128, 6, 0, 1);
	game.load.spritesheet('apricot', 'assets/entities/Apricot/apricot.png', 38, 54, 6, 0, 1);
	game.load.spritesheet('boxka', 'assets/entities/Boxka/boxka.png', 87, 46, 5, 0, 1);
	
	game.load.spritesheet('gumpy', 'assets/entities/Gumpy/gumpy.png', 32, 64, 8, 0, 1);
	game.load.spritesheet('spring', 'assets/entities/Spring/spring.png', 32, 16, 4, 0, 1);
	
	game.load.image('platform', 'assets/tiles/platform0.png');
	game.load.image('crate', 'assets/entities/Crate/crate.png');
	
	game.load.image('key', 'assets/entities/Key/key.png');
	game.load.image('sparkle', 'assets/entities/Key/sparkle.png');
	game.load.image('sparklebig', 'assets/entities/Key/sparklebig.png');
	
	game.load.image('ball', 'assets/placeholder/catball.png');
	
	game.load.image('blue slab', 'assets/tiles/blue_slab.png')
	game.load.image('green slab', 'assets/tiles/green_slab.png')
	
	/*game.load.tilemap('test map', 'assets/tilemaps/test_0.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('test tiles', 'assets/tiles/blue_tile.png');
    game.load.image('test gumpy', 'assets/entities/Gumpy/gumpyidle0.png');*/
	
	game.cache.removeTilemap('test map');
	//game.load.tilemap('test map', 'assets/tilemaps/test_1.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('area1', 'assets/tilemaps/area1.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('area2', 'assets/tilemaps/area2.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.image('test tiles', 'assets/tiles/blue_tile.png');
	game.load.image('test tiles2', 'assets/tiles/green_tile.png');
	game.load.image('door', 'assets/entities/Door/door.png');
	game.load.image('door_h', 'assets/entities/Door/door_h.png');
	game.load.image('spikes blue', 'assets/entities/Spikes/spikes_blue.png');
    game.load.image('test gumpy', 'assets/entities/Gumpy/PNG/gumpyidle0.png');
	game.load.image('yellow pop', 'assets/tiles/yellow_pop.png');
	game.load.image('pink pop', 'assets/tiles/pink_pop.png');
	game.load.image('blue pop', 'assets/tiles/blue_pop.png');
	game.load.image('green pop', 'assets/tiles/green_pop.png');
	game.load.image('jt', 'assets/tiles/jt.png');
},
create: function() { 
	game.state.start('menu');
}
}

GAME_WIDTH = 1024; GAME_HEIGHT = 640;
var game = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.CANVAS, 'pmachine');
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('game', gameState);
game.state.start('load');
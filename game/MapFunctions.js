function mapInit() {
	// Background slab - must be drawn after player
	bgSlab = game.add.tileSprite(0, 0, GAME_WIDTH, GAME_HEIGHT, 'blue slab');
	bgSlab.fixedToCamera = true;
	// bgSlab.alpha = 0;
	dirtyTiles = false;
	
	roomBgs = game.add.group();
	roomBgs.add(new Phaser.TileSprite(game, 0,0,0,0, 'yellow pop'));
	roomBgs.add(new Phaser.TileSprite(game, 0,0,0,0, 'yellow pop'));
	currentRoomBg = roomBgs.getAt(0);
	
	// roomObjects, all objects in a room, to help with resetting the room
	roomObjects = [player1];
	mapNum = 2;
	roomNum = 1;
	singleRoomTiles = [];
	setMap(mapNum);
	
	transitionNextMap = false;
	
	fadeInRoom(getRoomRect(roomNum.toString()));
	
	tiles.resizeWorld();	
}

function setMap(num) {
	map = this.game.add.tilemap('area' + num);
	if (num == 1) {
		map.addTilesetImage('blue', 'test tiles');
		bgSlab.loadTexture('blue slab');
		roomBgs.getAt(0).loadTexture('yellow pop');
		roomBgs.getAt(1).loadTexture('yellow pop');
	} else if (num == 2) {
		map.addTilesetImage('blue', 'test tiles2');
		bgSlab.loadTexture('green slab');
		roomBgs.getAt(0).loadTexture('green pop');
		roomBgs.getAt(1).loadTexture('green pop');
	}
	map.addTilesetImage('jt', 'jt');
	map.addTilesetImage('playerspawn', 'test gumpy');
	map.setCollisionBetween(0, 30, true, 'Tiles', true);
	map.setCollisionBetween(0, 30, true, 'TilesExtra', true);
	
	tiles = map.createLayer('Tiles');
	tilesExtra = map.createLayer('TilesExtra');
	entityLayer = map.objects['Entities'];
		
	makeTilesInvisible(tiles);
	makeTilesInvisible(tilesExtra);
}

function mapUpdate() {
	// Update map stuff!
	// Keep tiles dirty if doing something that changes the tiles (Do I need to?)
	if (dirtyTiles)
		//tiles.dirty = true;
	
	bgSlab.tilePosition.x = -game.camera.x;
	bgSlab.tilePosition.y = -game.camera.y;
	
	// Room bg parallax (which i don't want right now)
	/*roomBgs.getAt(0).tilePosition.set(game.camera.x * -0.3, game.camera.y * -0.3)
	roomBgs.getAt(1).tilePosition.set(game.camera.x * -0.3, game.camera.y * -0.3)*/
}

function makeTilesInvisible(tiles) {
	// Fade out all tiles on a layer
	var w = 32*tiles.map.width; var h = 32*tiles.map.height;
	tiles.getTiles(0, 0, w, h, false, false)
	.forEach(function(tile) {
		//game.add.tween(tile).to( { alpha: 0.0 }, 3000, "Linear", true);
		tile.alpha = 0;
	});
}

function makeLastTilesInvisible(quickFade) {
	// Fade out the tiles in the room before the room the player is in
	if (quickFade)
		var delay = 0;
	else
		var delay = 800;
	singleRoomTiles.forEach(function(tile) {
		game.add.tween(tile).to( { alpha: 0 }, 0, null, true, delay);
	});
	singleRoomTiles = [];
}

function nextRoom(door, newMap=false) {
	roomNum++;
	var roomRect = getRoomRect(roomNum.toString());
	if (roomNum > 0) {
		if (roomRect)
			makeLastTilesInvisible(false);
		else
			makeLastTilesInvisible(true);
		fadeOutObjects();
	}
	if (roomRect) {
		fadeInRoom(roomRect, newMap);
		if (door)
			player1.startMoveToNextRoom(door.isVertical(), roomRect);
	} else {
		// No room rect, must be end of map. Go to next one.
		endOfMap(true);
	}
}

function getRoomRect(roomNum) {
	var roomRect = null;
	entityLayer.forEach(function(o) {
		if (o.type == 'Room' && o.name == roomNum) {
			roomRect = o;
			return;
		}
	});
	return roomRect;
}

function fadeInRoom(roomRect, newMap=false) {
	if (newMap)
		var delay = 1500;
	else
		var delay = 0;
	// Fade in tiles in area 0
	if (roomRect) {
		// Fade in tiles
		keepTilesDirty(true);
		var w = roomRect.width; var h = roomRect.height;
		var x = roomRect.x; var y = roomRect.y;
		var addCallback = true;
		tiles.getTiles(x, y, w, h, false, false)
		.forEach(function(tile) {
			if (newMap)
				game.add.tween(tile).to( { alpha: 1.0 }, 1000, "Linear", true, delay);
			else
				tile.alpha = 1;
			singleRoomTiles.push(tile);
			setTileProperties(tile);
			if (tile.properties.door_h)
				console.log(tile.width);
		});
		tilesExtra.getTiles(x, y, w, h, false, false)
		.forEach(function(tile) {
			if (newMap)
				game.add.tween(tile).to( { alpha: 1.0 }, 1000, "Linear", true, delay);
			else
				tile.alpha = 1;
			singleRoomTiles.push(tile);
			setTileProperties(tile);
			if (tile.properties.door_h)
				console.log(tile.width);
		});
		
		// Spawn objects for the first time
		/* For some reason in Tiled, object Rectangles' origin pos is their top left, but for tile stamp things it's their bottom left*/
		// Commented out line below - not every objects is cleared
		// from roomObjects anymore
		// roomObjects = [player1];
		for (i = 0; i < entityLayer.length; ++i) {
			var o = entityLayer[i];
			if (o.type != "Room") {
				if (o.x >= roomRect.x && o.x+o.width <= roomRect.x 	+ roomRect.width &&
					o.y-o.height >= roomRect.y && o.y <= roomRect.y + roomRect.height) {
					if ((newO = createObject(o)))
						roomObjects.push(newO);
					entityLayer.splice(i, 1);
					i--;
				}	
			}
		}
		restartCooldown = 2.5;
		
		if (currentRoomBg == roomBgs.getAt(0)) {
			// use second one
			var roomBg = roomBgs.getAt(1);
		} else {
			// use first one
			var roomBg = roomBgs.getAt(0);
		}
		roomBg.position.x = x;
		roomBg.position.y = y;
		roomBg.width = w;
		roomBg.height = h;
		game.add.tween(roomBg).to( { alpha: 1.0 }, 800, "Linear", true, delay);
		game.add.tween(currentRoomBg).to( { alpha: 0 }, 1000, "Linear", true, 1000);
		currentRoomBg = roomBg;
		if (newMap)
			game.add.tween(bgSlab).to( { alpha: 1 }, 1000, "Linear", true, delay);

	}
}

function endOfMap(firstCall) {
	if (firstCall) {
		game.add.tween(bgSlab).to( { alpha: 0.0 }, 1000, "Linear", true);
		game.add.tween(currentRoomBg).to( { alpha: 0.0 }, 1000, "Linear", true);
		transitionNextMap = true;
		player1.endOfMap(true);
	} else {
		player1.endOfMap(false);
	}
	
	if (player1.readyForNextMap) {
		// Go to next map
		transitionNextMap = false;
		//game.camera.unfollow();
		setMap(++mapNum);
		roomNum = -1;
		player1.needsPosition = true;
		player1.readyForNextMap = false;
		nextRoom(null, true);
	}
}

function setTileProperties(tile) {
	if (tile.properties.jt) {
		tile.setCollision(false,false,true,false);
	}   
}       

function restartRoom(force=false) {
	if ((restartCooldown > 0 && !force) || !(game.camera.target == player1) || transitionNextMap)
		return;
	
	pausePhysics(false);
	// Move all objects back to initial positions and then reset them
	roomObjects.forEach(function(o) {
		o.reset_();
		if (o.name != 'Spike')
			o.body.moves = false;
	});
	resetting = true;
}

function keepTilesDirty(keepDirt) {
	dirtyTiles = keepDirt;
}

function lookAtDoor(fromDoor=false) {
	/****TEMPORARY***/
	return;
	/****/
	if (!fromDoor) {
		game.camera.unfollow(player1);
		dX = currentDoor.x + currentDoor.width/2;
		dY = currentDoor.y + currentDoor.height/2;
		game.add.tween(cameraDoorSprite.position).to( { x:dX, y:dY }, 1750, Phaser.Easing.Quadratic.Out, true);
		game.camera.follow(cameraDoorSprite);
	} else {
		pX = player1.x;
		pY = player1.y;
		t=game.add.tween(cameraDoorSprite.position).to( { x:pX, y:pY }, 1750, Phaser.Easing.Quadratic.Out, true);
		t.onComplete.add(lookAtPlayerFromDoor);
	}	
}

function lookAtPlayerFromDoor() {
	game.camera.unfollow(cameraDoorSprite);
	game.camera.follow(player1);
	freezePhysics = false;
	pausePhysics(false);
}

function checkDoorCondition() {
	var pX = player1.body.x; var pY = player1.body.y;
	var dX = currentDoor.x; var dY = currentDoor.y;
	if (mapNum == 1) {
		switch (roomNum) {
		case 0:
			return (pX >= dX - 500 && pY <= dY+currentDoor.height);
		case 1:
			return (pX > dX - 80 && pY <= dY);
		case 2:
			return (pY > dY - 80 && pX <= dX + 160);
		case 3:
			return (pX > dX-64);
		case 4:
			return currentDoor.hasKey;
		case 5:
			return (pY < dY+160);
		case 6:
			return (pX > dX-64);
		case 7:
			return (currentDoor.hasKey);
		case 8:
			return (pX >= dX-128 && pY >= dY);
		}
	}
	return false;
}